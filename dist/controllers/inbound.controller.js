"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const appeng_core_1 = require("appeng-core");
const info_commons_1 = require("info-commons");
const cloneDeep_1 = __importDefault(require("lodash/cloneDeep"));
const graphql_query_1 = require("../queries/graphql.query");
class InboundController {
    async insert(request, response) {
        const tokenResponse = await appeng_core_1.validateToken(request.get(appeng_core_1.TOKEN_KEY));
        if (tokenResponse instanceof appeng_core_1.ErrorResponse) {
            return response.status(tokenResponse.code).json(tokenResponse);
        }
        const referenceData = cloneDeep_1.default(tokenResponse);
        appeng_core_1.tokenClaims.map((key) => delete referenceData[key]);
        const data = await info_commons_1.getMetaData(info_commons_1.APPENG_META_URI, request.params.compositeEntityName, graphql_query_1.query, request.get(appeng_core_1.TOKEN_KEY));
        let jsonBody;
        try {
            jsonBody = createRequestBody(data, request.body);
        }
        catch (err) {
            const errorRes = info_commons_1.errorResponse(400, 'Insufficient Request Data', 'domain', 'Insufficient Data', 'Data Required', 'Request Body', 'Body', 'http://request/help');
            return response.status(errorRes.code).json(errorRes);
        }
        const entityGroupData = await appeng_core_1.transformToLogicalData(JSON.parse(jsonBody), referenceData);
        const validator = appeng_core_1.AppengValidationConfig.INSTANCE;
        const errorResp = await validator.validateData(entityGroupData);
        if (errorResp.errors.length === 0) {
            const inboundDataService = appeng_core_1.AppengCoreConfig.INSTANCE;
            const result = await inboundDataService.process(entityGroupData);
            return response.status(201).json(result);
        }
        return response.status(406).json(errorResp);
    }
}
const createRequestBody = (data, body) => {
    return JSON.stringify({
        action: 'Save',
        method: null,
        baseEntity: {
            name: data.CompositeEntity.rootCompositeEntityNode.entity.name,
            configId: data.CompositeEntity.rootCompositeEntityNode.entity.configObjectId,
            nodeId: data.CompositeEntity.rootCompositeEntityNode.configObjectId,
            records: body,
            childEntities: [],
        },
    });
};
const inboundController = new InboundController();
Object.freeze(inboundController);
exports.default = inboundController;
//# sourceMappingURL=inbound.controller.js.map